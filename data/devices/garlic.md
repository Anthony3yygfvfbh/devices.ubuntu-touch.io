---
name: "Yu Yureka Black"
comment: "wip"
deviceType: "phone"
maturity: .7
image: "https://fdn2.gsmarena.com/vv/pics/yureka/yu-yureka-yu5040-1.jpg"
description: 'The "garlic" flavour of handset were released around the world with various branding, providing a stylish mid-range phone at a reasonable price. The Yu Yureka Black is a 5.0" phone with a 1080x1920p resolution display. The Snapdragon 430 chipset is paired with 4GB of RAM and 32GB of storage. The main camera is 13MP and the selfie camera is 8MP. The battery has a 3000mAh capacity. This port is compatible with Wiko Ufeel Prime, YU Yureka Black (YU5040), Smart HERO II, Casper VIA M2 and the BLU Life One X2 Mini.'

externalLinks:
  - name: "Forum Post"
    link: "https://forums.ubports.com/topic/3702/yu-black-garlic"
    icon: "yumi"
---
