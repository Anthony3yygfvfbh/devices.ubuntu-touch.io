---
name: "Samsung Galaxy S5"
deviceType: "phone"
description: "This phone is the last high-end Samsung phone from which you can replace the battery without any tools!"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "-"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "+"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "?"
      - id: "dualSim"
        value: "x"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "-"
      - id: "pinUnlock"
        value: "?"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "?"
      - id: "voiceCall"
        value: "-"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "-"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "?"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "-"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "-"
      - id: "onlineCharging"
        value: "+-"
      - id: "recoveryImage"
        value: "?"
      - id: "factoryReset"
        value: "?"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "-"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "?"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "?"
      - id: "flightMode"
        value: "?"
      - id: "hotspot"
        value: "?"
      - id: "nfc"
        value: "?"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "-"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "?"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "?"
      - id: "adb"
        value: "?"
      - id: "wiredExternalMonitor"
        value: "?"
deviceInfo:
  - id: "cpu"
    value: "Quad-core 2.5 GHz Krait 400"
  - id: "chipset"
    value: "Qualcomm MSM8974AC Snapdragon 801"
  - id: "gpu"
    value: "Adreno 330"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "2GB"
  - id: "android"
    value: "Android 4.4.2 (KitKat), upgradable to 6.0 (Marshmallow)"
  - id: "battery"
    value: "2800 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.1 in"
  - id: "rearCamera"
    value: "16MP"
  - id: "frontCamera"
    value: "2MP"
contributors:
  - name: RealTheHexagon
    forum: https://forums.ubports.com/user/thehexagon
    github: https://github.com/RealTheHexagon
    gitlab: https://gitlab.com/RealTheHexagon
---

## Installation instructions

Before continuing, you should read these pages to have an understanding of how Ubuntu Touch, and Halium works:

- [Halium Documentation](https://docs.halium.org/en/latest/index.html),
- [Ubuntu Touch Documentation](https://docs.ubports.com/en/latest/).

---

**Warning!**

I am not responsible for your bricked device in any way, if you do not read the instructions.

---

### Prerequisites

To install Ubuntu Touch on your Samsung Galaxy S5, the following things are required:

- A Linux computer, as I don't know how to use Odin to flash things to the BOOT partition,
- [An unlocked bootloader](https://www.quora.com/How-can-I-unlock-the-bootloader-of-my-SMG900F),
- [TWRP](https://dl.twrp.me/klte/) installed on your phone, for
- [Heimdall](https://glassechidna.com.au/heimdall/), to flash the kernel onto the BOOT partition
- [ADB](https://www.xda-developers.com/install-adb-windows-macos-linux/), to flash things onto your phone,
- Courage, as there is a small chance you may brick your device.

If you have all of these, **congratulations**, you can install Ubuntu Touch onto your phone.

---

### Installation

1. Download this [folder](https://drive.google.com/drive/folders/1TsroZoC0L3zuV-td-0sLswZdtiBT64-T?usp=sharing), which has the images and rootfs within it,

2. Clone the halium-install repo, which has all the scripts required to install UBTouch, with: `git clone https://gitlab.com/JBBgameich/halium-install.git`,

3. Make a backup of all your data, if you want to, by booting into the recovery, and selecting `backup`, and backing up onto a external drive, such as a Micro SD card, and make sure to have a Android image downloaded in case you want to switch back,

4. Wipe the device, by going to `Wipe > Advanced Wipe` and selecting:

   - Dalvik / ART Cache,
   - System,
   - Data,
   - Internal Storage,
   - Cache

     Make sure you have selected the right things, and you want to wipe your device, and then swipe to wipe.

5. Reboot your device into the download mode, and run this command: `heimdall flash --BOOT path/to/halium-boot.img`, making sure to replace `path/to/halium-boot.img` with the path to the `halium-boot.img` file. This is to flash the kernel of Ubuntu Touch.

6. As soon as the flashing has completed, make sure to boot back into recovery, making sure the device doesn't boot normally, i.e into the system.

7. Flash the system.img and rootfs together with: `path/to/halium-install/script -p ut path/to/rootfs.tar.gz path/to/system.img`, while the phone is booted in the recovery.

8. While the script is running, it will ask you to enter a password. This password will be the password that you will unlock the device with, so please remember this password.

9. You are done! Reboot into the system by going `Reboot > System`, making sure to accept that there is no OS installed, which there is, and denying to install the TWRP app. When booting up, you should see the Unity8 Logo, and soon see the setup wizard.

If anything goes wrong, eg the phone stays on the Unity8 Logo, please let me know by putting a issue on [this repo](https://github.com/RealTheHexagon/klte-ubports-install-instructions), and describing where it goes wrong.
