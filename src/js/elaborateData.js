module.exports = {
  loadSources: loadSources,
  fetchData: fetchData,
  importData: importData,
  run: elaborate,
  clean: clean
};

const portStatus = require("../../data/portStatus.json");
const deviceInfo = require("../../data/deviceInfo.json");
const progressStages = require("../../data/progressStages.json");
const ignoredCommits = require("../../data/ignoredCommits.json");
const portType = require("../../data/portType.json");
const releaseChannelDescriptions = require("../../data/releaseChannelDescriptions.json");

const Axios = require("axios");
const slugify = require("@sindresorhus/slugify");
const dataUtils = require("./dataUtils.js");
const decode = require("html-entities").decode;

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

async function loadSources(api) {
  // Generated redirects are stored in the NetlifyRedirects collection for the redirects plugin
  const netlifyRedirects = api.addCollection("NetlifyRedirects");

  await getInstallerUrls(netlifyRedirects);
}

async function fetchData(api, addSchemaTypes) {
  const collection = api._app.store.getCollection("Device");
  const installerData = await downloadInstallerData();
  const releaseChannelData = await downloadReleaseChannelData();
  createForumSchema(addSchemaTypes);

  for (let device of collection.data()) {
    getInstallerSupport(device, installerData);
    addProgressWeight(device);
    await getReleaseChannels(device, releaseChannelData);
    await downloadForumData(device);
  }
}

function importData(device, installerData) {
  // Set codename from file name
  device.codename = device.fileInfo.name;
  removeIgnoredCommits(device);
}

function elaborate(device) {
  // Set feature default and global values
  if (device.portStatus) {
    addMissingFeatures(device);
    addGlobalState(device);
    addConditionalStates(device);
    deleteUnavailableFeatures(device);
  }

  // Calculate progress and stages
  calculateProgress(device);
  calculateProgressStage(device);
  calculateProgressAdvancement(device);
  countUntestedFeatures(device);

  // Check wired display and bluetooth for convergence
  isConvergenceEnabled(device);

  // Add port type description from port type
  addPortTypeDescription(device);
}

function clean(device) {
  if (device.portStatus) {
    useNameInsteadOfFeatureId(device);
    deleteVoidCategories(device);
  }

  if (device.deviceInfo) {
    useNameInsteadOfSpecificationId(device);
  }
}

/* Functions */

// Get installer URLs for download from Github
async function getInstallerUrls(netlifyRedirects) {
  let githubLinks;

  try {
    githubLinks = await Axios.get(
      "https://api.github.com/repos/ubports/ubports-installer/releases/latest"
    );

    // Save links to NetlifyRedirects collection
    for (let el of ["exe", "deb", "dmg", "appimage"]) {
      let targetUrl = getInstallerUrlForPackage(el, githubLinks);
      netlifyRedirects.addNode({
        from: "/installer package=" + el,
        to: targetUrl,
        status: "302!"
      });
      netlifyRedirects.addNode({
        from: "/installer/" + el,
        to: targetUrl,
        status: "302"
      });
    }
  } catch (e) {
    // Display a warning if installer data wasn't downloaded
    if (process.argv.includes("offline")) {
      console.log(
        ansiCodes.yellowFG + "%s" + ansiCodes.reset,
        "Failed to get installer download link!\n",
        "- If you need to fetch installer download link connect to the internet."
      );
    } else {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Failed to get installer download link!\n",
        "- If you want to ignore this error add the 'offline' parameter."
      );
      process.exit(2);
    }
  }

  // The link for the snap package is not stored at Github ( direct link is provided )
  netlifyRedirects.addNode({
    from: "/installer package=snap",
    to: "https://snapcraft.io/ubports-installer",
    status: "302!"
  });
  netlifyRedirects.addNode({
    from: "/installer/snap",
    to: "https://snapcraft.io/ubports-installer",
    status: "302"
  });
}

// Get links from Github
function getInstallerUrlForPackage(packageType, githubLinks) {
  try {
    return githubLinks.data.assets.find((asset) =>
      asset.name.toLowerCase().endsWith(packageType.toLowerCase())
    ).browser_download_url;
  } catch (e) {
    console.log(e);
    return "/installer";
  }
}

// Download installer data
async function downloadInstallerData() {
  try {
    const response = await Axios({
      url: "https://ubports.github.io/installer-configs/v2/",
      method: "GET"
    });
    return response.data;
  } catch (e) {
    // Display a warning if installer data wasn't downloaded
    if (process.argv.includes("offline")) {
      console.log(
        ansiCodes.yellowFG + "%s" + ansiCodes.reset,
        "Installer data unavailable at build time.\n",
        "- If you need to fetch installer data connect to the internet."
      );
      return [];
    } else {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Failed to download installer data!\n",
        "- If you want to ignore this error add the 'offline' parameter."
      );
      process.exit(2);
    }
  }
}

// Create forum schema
function createForumSchema(addSchemaTypes) {
  addSchemaTypes(`
    type Device_ForumTopics @infer {
      title: String
      user: String
      timestamp: Float
      views: Int
      link: String
      locked: Int
      pinned: Int
    }
  `);

  addSchemaTypes(`
    type Device implements Node @infer {
      forumTopics: [Device_ForumTopics]
    }
  `);
}

// Download forum data
async function downloadForumData(device) {
  if (device.subforum) {
    try {
      const response = await Axios({
        url: "https://forums.ubports.com/api/category/" + device.subforum,
        method: "GET"
      });
      device.forumTopics = response.data.topics.map((topic) => {
        return {
          title: decode(topic.title),
          user: decode(topic.user.username),
          timestamp: topic.timestamp,
          views: topic.viewcount,
          link: "https://forums.ubports.com/topic/" + topic.slug,
          locked: topic.locked,
          pinned: topic.pinned
        };
      });
    } catch (e) {
      console.log(
        ansiCodes.redFG + "%s" + ansiCodes.reset,
        "Forum topic data download failed for: " + device.codename
      );
      device.forumTopics = [];
    }
  } else {
    device.forumTopics = [];
  }
}

// Download release channel data
async function downloadReleaseChannelData() {
  try {
    const response = await Axios({
      url: "https://system-image.ubports.com/channels.json",
      method: "GET"
    });
    return response.data;
  } catch (e) {
    // Display a warning if release channel data wasn't downloaded
    console.log(
      ansiCodes.yellowFG + "%s" + ansiCodes.reset,
      "Failed to download release channel data."
    );
    return {};
  }
}

// Download release channel data
async function downloadCurrentChannelData(path) {
  try {
    const response = await Axios({
      url: "https://system-image.ubports.com" + path,
      method: "GET"
    });
    return response.data;
  } catch (e) {
    // Display a warning if release channel data wasn't downloaded
    console.log(
      ansiCodes.yellowFG + "%s" + ansiCodes.reset,
      "Failed to download release channel data."
    );
    return {};
  }
}

// Add weight to installer and port data
function addProgressWeight(device) {
  device.progress = device.noInstall ? device.progress * 0.9 : device.progress;
  device.progress = device.portStatus
    ? device.progress
    : device.progress * 0.95;
  device.progress = Math.round(1000 * device.progress) / 10;
}

// Add missing features from defaults
function addMissingFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (!graphQlFeature) {
      // Don't add unavailable features
      if (feature.default != "x") {
        category.features.push({
          id: feature.id,
          name: feature.name,
          value: feature.default ? feature.default : "?"
        });
      }
    }
  });
}

// Check global state of the feature
function addGlobalState(device) {
  let featureScale = ["x", "-", "?", "+-", "+"];

  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (
      graphQlFeature &&
      !graphQlFeature.overrideGlobal &&
      feature.global &&
      featureScale.indexOf(feature.global) <=
        featureScale.indexOf(graphQlFeature.value)
    ) {
      graphQlFeature.value = feature.global;
      graphQlFeature.bugTracker = feature.bugTracker ? feature.bugTracker : "";
      graphQlFeature.global = true;
    }
  });
}

// Mark state of the feature based on other conditions (mostly port type)
function addConditionalStates(device) {
  let featureScale = ["x", "-", "?", "+-", "+"];

  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (feature.conditions) {
      for (let condition of feature.conditions) {
        if (
          graphQlFeature &&
          !graphQlFeature.overrideGlobal &&
          device[condition.field] == condition.value &&
          featureScale.indexOf(condition.global) <=
            featureScale.indexOf(graphQlFeature.value)
        ) {
          graphQlFeature.value = condition.global;
          graphQlFeature.bugTracker = condition.bugTracker
            ? condition.bugTracker
            : "";
          graphQlFeature.global = true;
        }
      }
    }
  });
}

// Delete unavailable features
function deleteUnavailableFeatures(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature && graphQlFeature.value == "x") {
      category.features.splice(category.features.indexOf(graphQlFeature), 1);
    }
  });
}

// Get installer compatibility data
function getInstallerSupport(device, installerData) {
  let deviceCodenameAlias = device.installerAlias
    ? device.installerAlias
    : device.codename;
  let deviceInstaller = installerData.some(
    (el) =>
      el.codename == deviceCodenameAlias &&
      el.operating_systems.includes("Ubuntu Touch")
  );
  device.noInstall = !deviceInstaller;
}

// Get release channel data
async function getReleaseChannels(device, releaseChannelData) {
  let allChannels = [];
  let deviceCodenameAlias = device.installerAlias
    ? device.installerAlias
    : device.codename;
  for (let chID in releaseChannelData) {
    if (
      !releaseChannelData[chID].hidden &&
      releaseChannelData[chID].devices[deviceCodenameAlias]
    ) {
      let currentChannel = await downloadCurrentChannelData(
        releaseChannelData[chID].devices[deviceCodenameAlias].index
      );
      if (!currentChannel.images.length) continue;
      let lastOtaRelease = currentChannel.images[
        currentChannel.images.length - 1
      ].version_detail
        .split(",")
        .reduce((previousValue, currentValue, currentIndex, array) => {
          previousValue[currentValue.split("=").shift()] = currentValue
            .split("=")
            .pop();
          return previousValue;
        }, {});
      let channelName = chID.replace(/^(ubports\-touch\/)/, "");
      let releaseDate =
        lastOtaRelease.ubports.slice(0, 4) +
        "-" +
        lastOtaRelease.ubports.slice(4, 6) +
        "-" +
        lastOtaRelease.ubports.slice(6, 8);
      let ubuntuRelease = channelName.split("/").shift();
      allChannels.push({
        channel: channelName.split("/").pop(),
        fullName: chID,
        ubuntuRelease:
          ubuntuRelease == "16.04"
            ? "Xenial"
            : ubuntuRelease == "20.04"
            ? "Focal"
            : ubuntuRelease,
        version: lastOtaRelease.tag,
        releaseDate: new Date(releaseDate),
        description: releaseChannelDescriptions[channelName.split("/").pop()]
      });
    }
  }
  device.releaseChannels = allChannels;
}

// Calculate porting progress from feature matrix ( use maturity field as fallback )
function calculateProgress(device) {
  if (device.portStatus) {
    let totalWeight = 0,
      currentWeight = 0;

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature) {
        totalWeight += feature.weight;
        currentWeight += graphQlFeature.value == "+" ? feature.weight : 0;
      }
    });

    device.progress = currentWeight / totalWeight;
  } else {
    // Fallback from maturity
    device.progress = device.maturity;
  }
}

// Count number of untested features
function countUntestedFeatures(device) {
  if (device.portStatus) {
    let untestedCount = 0;

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature) {
        untestedCount += graphQlFeature.value == "?" ? 1 : 0;
      }
    });

    device.untestedCount = untestedCount;
  } else {
    device.untestedCount = 0;
  }
}

// Calculate progress stage
function calculateProgressStage(device) {
  if (device.portStatus) {
    let currentStageIndex = progressStages.length - 1; // Daily-driver ready

    dataUtils.forEachFeature(device, function (feature, category) {
      let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

      if (graphQlFeature && graphQlFeature.value != "+") {
        if (currentStageIndex >= feature.stage && feature.stage > 0) {
          currentStageIndex = feature.stage - 1;
        }
      }
    });

    device.progressStage = {
      number: currentStageIndex,
      name: progressStages[currentStageIndex].name,
      description: progressStages[currentStageIndex].description
    };
  } else {
    // Fallback from maturity
    device.progressStage = {
      number: 0,
      name: progressStages[0].name,
      description: progressStages[0].description
    };
  }
}

// Calculate progress advancement score
function calculateProgressAdvancement(device) {
  let maxInactivityTime = new Date();
  maxInactivityTime.setMonth(maxInactivityTime.getMonth() - 6); // Use 6 months inactivity time

  let lastCommit = new Date(
    device.gitData[0].authorDate.replace(/\+(\w{2})(\w{2})/, "+$1:$2")
  );

  if (device.tag == "unmaintained") device.progressAdvancement = "Abandoned";
  else if (device.progressStage.number == progressStages.length - 1)
    device.progressAdvancement = "Complete";
  else if (lastCommit > maxInactivityTime)
    device.progressAdvancement = "Active";
  else device.progressAdvancement = "Inactive";
}

// Add wired convergence availability
function isConvergenceEnabled(device) {
  try {
    let wiredExternalMonitor = dataUtils.getFeatureById(
      dataUtils.getCategoryByName(device, "USB"),
      "wiredExternalMonitor"
    );

    let bluetooth = dataUtils.getFeatureById(
      dataUtils.getCategoryByName(device, "Network"),
      "bluetooth"
    );

    device.isConvergenceEnabled =
      (bluetooth.value == "+" || bluetooth.value == "+-") &&
      (wiredExternalMonitor.value == "+" || wiredExternalMonitor.value == "+-");
  } catch (e) {
    device.isConvergenceEnabled = false;
  }
}

// Add port type description
function addPortTypeDescription(device) {
  if (device.portType) {
    device.portTypeDescription = portType[device.portType]
      ? portType[device.portType]
      : portType["Unknown"];
  } else {
    device.portTypeDescription = portType["Unknown"];
  }
}

// Replace ID with name
function useNameInsteadOfFeatureId(device) {
  dataUtils.forEachFeature(device, function (feature, category) {
    let graphQlFeature = dataUtils.getFeatureById(category, feature.id);

    if (graphQlFeature) {
      graphQlFeature.name = feature.name;
      delete graphQlFeature.id;
    }
  });
}

// Delete categories that have no features set
function deleteVoidCategories(device) {
  for (let portCategory in portStatus) {
    let category = dataUtils.getCategoryByName(device, portCategory);

    if (category && category.features.length == 0) {
      device.portStatus.splice(device.portStatus.indexOf(category), 1);
    }
  }
}

// Elaborate device info from IDs
function useNameInsteadOfSpecificationId(device) {
  device.deviceInfo.forEach((el) => {
    el.name = deviceInfo.find((info) => info.id == el.id).name;
    delete el.id;
  });
}

// Remove development commits from the data history
function removeIgnoredCommits(device) {
  for (let commit = device.gitData.length - 2; commit >= 0; commit--) {
    if (ignoredCommits.includes(device.gitData[commit].hash)) {
      device.gitData.splice(commit, 1);
    }
  }
}
